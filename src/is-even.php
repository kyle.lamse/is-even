<?php
if (!function_exists('is_even')) {
    /**
     * @param mixed $value
     * @return bool
     */
    function is_even($value) {
        return $value % 2 === 0;
    }
}