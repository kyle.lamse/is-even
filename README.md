# is-even
Determines if a given value is even.

## Installation
```bash
composer require kylelamse/is-even
```
After this package has been installed, composer will autoload it.

## Usage
```php
is_even(2)    // Returns true
is_even(9)    // Returns false
is_even("4")  // Returns true
is_even("15") // Returns false
```
