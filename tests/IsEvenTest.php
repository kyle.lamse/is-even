<?php
use PHPUnit\Framework\TestCase;

class IsEvenTest extends TestCase
{
    public function testValidEvenInteger()
    {
        $this->assertTrue(is_even(2));
    }

    public function testValidOddInteger()
    {
        $this->assertFalse(is_even(5));
    }

    public function testValidEvenString()
    {
        $this->assertTrue(is_even("8"));
    }

    public function testValidOddString()
    {
        $this->assertFalse(is_even("11"));
    }

    public function testValidEvenFloat()
    {
        $this->assertTrue(is_even("14.0"));
    }

    public function testValidOddFloat()
    {
        $this->assertFalse(is_even("15.0"));
    }
}